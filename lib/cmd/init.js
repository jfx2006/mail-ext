import { relative, join, resolve } from "path"
import { open } from "fs/promises"
import { ensureDir } from "fs-extra"
import { inspect } from "util"
import { packageDirectory } from "pkg-dir"
import { isSubPath } from "../../node_modules/web-ext/lib/util/file-filter.js"
import fileExists from "../../node_modules/web-ext/lib/util/file-exists.js"

const MAGIC_CONFIG_FILENAME = "web-ext-config.js"

async function doConfig(config, sourceDir) {
  console.log(`Writing ${config}...`)
  const configJs = {
    build: {
      filename: "{name}-{version}.xpi",
    },
    run: {
      devtools: true,
      browserConsole: true,
    }
  }
  if (sourceDir !== null) {
    configJs.sourceDir = sourceDir
  }
  const file = await open(config, "w")
  await file.write("module.exports = ")
  await file.write(inspect(configJs))
  await file.write("\n")
  await file.close()
  console.log("Done!")
}

async function doManifest(manifestPath, manifestVersion) {
  console.log(`Writing ${manifestPath}...`)
  const manifest = {
    "manifest_version": manifestVersion,
    "name": "Mail Extension Name",
    "description": "A mail extension template.",
    "version": "1.0",
    "author": "HJ Simpson",
    "applications": {
      "gecko": {
        "id": "template@demo.example.com",
        "strict_min_version": "78.0"
      }
    }
  }
  const file = await open(manifestPath, "w")
  await file.write(JSON.stringify(manifest, null, 2))
  await file.close()
  console.log("Done!")
}

export default async function init({ sourceDir, config, manifestVersion }) {
  const pkgDir = await packageDirectory()
  if (!config) {
    config = MAGIC_CONFIG_FILENAME
  }
  config = resolve(pkgDir, config)
  if (await fileExists(config)) {
    throw new Error(`Refusing to overwrite existing ${config}.`)
  }

  const sourcePath = resolve(sourceDir)
  if (!isSubPath(pkgDir, sourcePath) && pkgDir !== sourceDir) {
    throw new Error(`${sourcePath} is not a subdirectory of ${pkgDir}.`)
  }
  let relativeSourceDir = null
  if (sourcePath !== pkgDir) {
    relativeSourceDir = relative(pkgDir, sourcePath)
  }
  const manifestPath = join(sourcePath, "manifest.json")
  if (await fileExists(manifestPath)) {
    throw new Error(`Refusing to overwrite existing manifest ${manifestPath}.`)
  }
  try {
    await ensureDir(sourcePath)
  } catch (err) {
    throw new Error(`Problem creating ${relativeSourceDir}.`)
  }
  await doConfig(config, relativeSourceDir)
  await doManifest(manifestPath, manifestVersion)
}
