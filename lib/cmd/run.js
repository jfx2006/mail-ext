import * as defaultThunderbirdApp from "../thunderbird/index.js"
import webextRun from "../../node_modules/web-ext/lib/cmd/run.js"

export default async function run(params, { firefoxApp = defaultThunderbirdApp } = {}) {
  params.target = "firefox-desktop"
  params.firefox = params.thunderbird
  if (params.thunderbirdProfile) {
    params.firefoxProfile = params.thunderbirdProfile
    delete params.thunderbirdProfile
  } else if (params["thunderbird-profile"]) {
    params.firefoxProfile = params["thunderbird-profile"]
    delete params["thunderbird-profile"]
  }
  for (const param of [
    "thunderbirdProfile",
    "thunderbird-profile",
    "thunderbird",
    "t",
    "thunderbird-binary",
    "thunderbirdBinary",
  ]) {
    if (Object.hasOwn(params, param)) {
      delete params[param]
    }
  }
  if (params.browserConsole) {
    params.args.push("--devtools")
    params.browserConsole = false
  }
  return webextRun(params, { firefoxApp })
}
