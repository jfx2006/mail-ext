import { cmd } from "web-ext"

const { build, docs, lint } = cmd

async function init(params) {
  const { default: runCommand } = await import("./init.js")
  return runCommand(params)
}

async function run(params, options) {
  const { default: runCommand } = await import("./run.js")
  return runCommand(params, options)
}

export default { init, build, docs, lint, run }
