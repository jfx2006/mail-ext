import path from "path"

import defaultCommands from "./cmd/index.js"
import { coerceCLICustomPreference } from "./thunderbird/preferences.js"

const envPrefix = "WEB_EXT"

// export const ATN_BASE_URL = 'https://addons.thunderbird.net/api/v4/';

import {
  Program,
  defaultVersionGetter,
  throwUsageErrorIfArray,
} from "../node_modules/web-ext/lib/program.js"

export async function main(
  absolutePackageDir,
  { getVersion = defaultVersionGetter, commands = defaultCommands, argv, runOptions = {} } = {},
) {
  const program = new Program(argv, { absolutePackageDir })
  const version = await getVersion(absolutePackageDir)

  // yargs uses magic camel case expansion to expose options on the
  // final argv object. For example, the 'artifacts-dir' option is alternatively
  // available as argv.artifactsDir.
  program.yargs
    .usage(
      `Usage: $0 [options] command

Option values can also be set by declaring an environment variable prefixed
with $${envPrefix}_. For example: $${envPrefix}_SOURCE_DIR=/path is the same as
--source-dir=/path.

To view specific help for any given command, add the command name.
Example: $0 --help run.
`,
    )
    .help("help")
    .alias("h", "help")
    .env(envPrefix)
    .version(version)
    .demandCommand(1, "You must specify a command")
    .strict()
    .recommendCommands()

  program.setGlobalOptions({
    "source-dir": {
      alias: "s",
      describe: "Mail extension source directory.",
      default: process.cwd(),
      requiresArg: true,
      type: "string",
      coerce: (arg) => (arg != null ? path.resolve(arg) : undefined),
    },
    "artifacts-dir": {
      alias: "a",
      describe: "Directory where artifacts will be saved.",
      default: path.join(process.cwd(), "mail-ext-artifacts"),
      normalize: true,
      requiresArg: true,
      type: "string",
    },
    verbose: {
      alias: "v",
      describe: "Show verbose output",
      type: "boolean",
      demandOption: false,
    },
    "ignore-files": {
      alias: "i",
      describe:
        "A list of glob patterns to define which files should be " +
        "ignored. (Example: --ignore-files=path/to/first.js " +
        'path/to/second.js "**/*.log")',
      demandOption: false,
      // The following option prevents yargs>=11 from parsing multiple values,
      // so the minimum value requirement is enforced in execute instead.
      // Upstream bug: https://github.com/yargs/yargs/issues/1098
      // requiresArg: true,
      type: "array",
    },
    "no-input": {
      describe: "Disable all features that require standard input",
      type: "boolean",
      demandOption: false,
    },
    input: {
      // This option is defined to make yargs to accept the --no-input
      // defined above, but we hide it from the yargs help output.
      hidden: true,
      type: "boolean",
      demandOption: false,
    },
    config: {
      alias: "c",
      describe: "Path to a CommonJS config file to set option defaults",
      default: undefined,
      demandOption: false,
      requiresArg: true,
      type: "string",
    },
    "config-discovery": {
      describe:
        "Discover config files in home directory and " +
        "working directory. Disable with --no-config-discovery.",
      demandOption: false,
      default: true,
      type: "boolean",
    },
  })

  program
    .command("init", "Initialize a mail extension", commands.init, {
      "manifest-version": {
        alias: "m",
        describe: "Manifest version to use",
        type: "number",
        choices: [2, 3],
        default: 2,
        defaultDescription: "Manifest version 2",
      }
    })
    .command("build", "Create an extension package from source", commands.build, {
      "as-needed": {
        describe: "Watch for file changes and re-build as needed",
        type: "boolean",
      },
      filename: {
        alias: "n",
        describe: "Name of the created extension package file.",
        default: undefined,
        normalize: false,
        demandOption: false,
        requiresArg: true,
        type: "string",
        coerce: (arg) =>
          arg == null
            ? undefined
            : throwUsageErrorIfArray("Multiple --filename/-n option are not allowed")(arg),
      },
      "overwrite-dest": {
        alias: "o",
        describe: "Overwrite destination package if it exists.",
        type: "boolean",
      },
    })
    .command("run", "Run the extension", commands.run, {
      thunderbird: {
        alias: ["t", "thunderbird-binary"],
        describe:
          "Path or alias to a Thunderbird executable such as thunderbird-bin " +
          "or thunderbird.exe. " +
          "If not specified, the default Thunderbird will be used. " +
          "You can specify the following aliases in lieu of a path: " +
          "thunderbird, beta, daily..",
        //'For Flatpak, use `flatpak:org.mozilla.firefox` where ' +
        // '`org.mozilla.firefox` is the application ID.',
        default: "thunderbird",
        demandOption: false,
        type: "string",
      },
      "thunderbird-profile": {
        alias: "p",
        describe:
          "Run Thunderbird using a copy of this profile. The profile " +
          "can be specified as a directory or a name, such as one " +
          "you would see in the Profile Manager. If not specified, " +
          "a new temporary profile will be created.",
        demandOption: false,
        type: "string",
      },
      "profile-create-if-missing": {
        describe: "Create the profile directory if it does not already exist",
        demandOption: false,
        type: "boolean",
      },
      "keep-profile-changes": {
        describe:
          "Run Thunderbird directly in custom profile. Any changes to " +
          "the profile will be saved.",
        demandOption: false,
        type: "boolean",
      },
      reload: {
        describe: "Reload the extension when source files change." + "Disable with --no-reload.",
        demandOption: false,
        default: true,
        type: "boolean",
      },
      "watch-file": {
        alias: ["watch-files"],
        describe:
          "Reload the extension only when the contents of this" +
          " file changes. This is useful if you use a custom" +
          " build process for your extension",
        demandOption: false,
        type: "array",
      },
      "watch-ignored": {
        describe:
          "Paths and globs patterns that should not be " +
          "watched for changes. This is useful if you want " +
          "to explicitly prevent web-ext from watching part " +
          "of the extension directory tree, " +
          "e.g. the node_modules folder.",
        demandOption: false,
        type: "array",
      },
      pref: {
        describe:
          "Launch thunderbird with a custom preference " +
          "(example: --pref=general.useragent.locale=fr-FR). " +
          "You can repeat this option to set more than one " +
          "preference.",
        demandOption: false,
        requiresArg: true,
        type: "array",
        coerce: (arg) => (arg != null ? coerceCLICustomPreference(arg) : undefined),
      },
      devtools: {
        describe: "Open the DevTools for the installed add-on " + "(Thunderbird 115 and later)",
        demandOption: false,
        type: "boolean",
      },
      "browser-console": {
        alias: ["bc"],
        describe: "Open the DevTools Console.",
        demandOption: false,
        type: "boolean",
      },
      args: {
        alias: ["arg"],
        describe: "Additional CLI options passed to the Thunderbird binary",
        default: [],
        demandOption: false,
        type: "array",
      },
    })
    .command("lint", "Validate the extension source", commands.lint, {
      output: {
        alias: "o",
        describe: "The type of output to generate",
        type: "string",
        default: "text",
        choices: ["json", "text"],
      },
      metadata: {
        describe: "Output only metadata as JSON",
        type: "boolean",
        default: false,
      },
      "warnings-as-errors": {
        describe: "Treat warnings as errors by exiting non-zero for warnings",
        alias: "w",
        type: "boolean",
        default: false,
      },
      pretty: {
        describe: "Prettify JSON output",
        type: "boolean",
        default: false,
      },
      privileged: {
        describe: "Treat your extension as a privileged extension",
        type: "boolean",
        default: false,
      },
      "self-hosted": {
        describe:
          "Your extension will be self-hosted. This disables messages " +
          "related to hosting on addons.thunderbird.net.",
        type: "boolean",
        default: false,
      },
      boring: {
        describe: "Disables colorful shell output",
        type: "boolean",
        default: false,
      },
    })
    .command("docs", "Open the web-ext documentation in a browser", commands.docs, {})

  return program.execute({ getVersion, ...runOptions })
}
