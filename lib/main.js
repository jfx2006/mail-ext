import { main } from "./program.js"
import cmd from "./cmd/index.js"

export default { main, cmd }
