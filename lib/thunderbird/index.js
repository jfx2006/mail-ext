import { getPrefs as defaultPrefGetter } from "./preferences.js"
import FirefoxProfile from "firefox-profile"
import {
  configureProfile as webextConfigureProfile,
  useProfile as webextUseProfile,
  createProfile as webextCreateProfile,
  copyProfile as webextCopyProfile,
  run,
  isDefaultProfile,
  defaultCreateProfileFinder,
  installExtension,
} from "../../node_modules/web-ext/lib/firefox/index.js"

const defaultUserProfileCopier = FirefoxProfile.copyFromUserProfile

export { run, isDefaultProfile, defaultCreateProfileFinder, installExtension }

export function configureProfile(
  profile,
  { app = "thunderbird", getPrefs = defaultPrefGetter, customPrefs = {} } = {},
) {
  return webextConfigureProfile(profile, { app, getPrefs, customPrefs })
}

export async function useProfile(
  profilePath,
  {
    app,
    configureThisProfile = configureProfile,
    isFirefoxDefaultProfile = isDefaultProfile,
    customPrefs = {},
    createProfileFinder = defaultCreateProfileFinder,
  } = {},
) {
  return await webextUseProfile(profilePath, {
    app,
    configureThisProfile,
    isFirefoxDefaultProfile,
    customPrefs,
    createProfileFinder,
  })
}

export async function createProfile({
  app,
  configureThisProfile = configureProfile,
  customPrefs = {},
} = {}) {
  return await webextCreateProfile({ app, configureThisProfile, customPrefs })
}

export async function copyProfile(
  profileDirectory,
  {
    app,
    configureThisProfile = configureProfile,
    copyFromUserProfile = defaultUserProfileCopier,
    customPrefs = {},
  } = {},
) {
  return await webextCopyProfile(profileDirectory, {
    app,
    configureThisProfile,
    copyFromUserProfile,
    customPrefs,
  })
}
