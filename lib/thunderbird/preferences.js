import {
  nonOverridablePreferences,
  getPrefs as getFFPrefs,
  coerceCLICustomPreference,
} from "../../node_modules/web-ext/lib/firefox/preferences.js"

// Prefs specific to Firefox for desktop.
const prefsFirefox = getFFPrefs("firefox")

const prefsThunderbird = {
  "app.donation.eoy.version.viewed": 99,
  "devtools.dom.enabled": true,
  "devtools.inspector.showUserAgentStyles": true,
  "devtools.toolbox.splitconsoleEnabled": true,

  // Set up bogus POP account and local folders
  "mail.account.account1.identities": "id1",
  "mail.account.account1.server": "server1",
  "mail.account.account2.server": "server2",
  "mail.account.lastKey": 2,
  "mail.accountmanager.accounts": "account1,account2",
  "mail.accountmanager.defaultaccount": "account1",
  "mail.accountmanager.localfoldersserver": "server2",

  "mail.identity.id1.archive_folder": "mailbox://mailuser%40example.com@example.com/Archives",
  "mail.identity.id1.doBcc": false,
  "mail.identity.id1.draft_folder": "mailbox://mailuser%40example.com@example.com/Drafts",
  "mail.identity.id1.drafts_folder_picker_mode": "0",
  "mail.identity.id1.fcc_folder": "mailbox://mailuser%40example.com@example.com/Sent",
  "mail.identity.id1.fcc_folder_picker_mode": "0",
  "mail.identity.id1.fullName": "Mail User",
  "mail.identity.id1.reply_on_top": 1,
  "mail.identity.id1.smtpServer": "smtp1",
  "mail.identity.id1.stationery_folder": "mailbox://mailuser%40example.com@example.com/Templates",
  "mail.identity.id1.tmpl_folder_picker_mode": "0",
  "mail.identity.id1.useremail": "mailuser@example.com",
  "mail.identity.id1.valid": true,
  "mail.server.server1.check_new_mail": false,
  "mail.server.server1.directory-rel": "[ProfD]Mail/example.com",
  "mail.server.server1.hostname": "127.0.0.1",
  "mail.server.server1.name": "mailuser@example.com",
  "mail.server.server1.port": 110,
  "mail.server.server1.socketType": 0,
  "mail.server.server1.spamActionTargetAccount": "mailbox://mailuser%40example.com@example.com",
  "mail.server.server1.storeContractID": "@mozilla.org/msgstore/berkeleystore;1",
  "mail.server.server1.type": "pop3",
  "mail.server.server1.userName": "mailuser@example.com",
  "mail.server.server2.directory-rel": "[ProfD]Mail/Local Folders",
  "mail.server.server2.hostname": "Local Folders",
  "mail.server.server2.name": "Local Folders",
  "mail.server.server2.storeContractID": "@mozilla.org/msgstore/berkeleystore;1",
  "mail.server.server2.type": "none",
  "mail.server.server2.userName": "nobody",
  "mail.smtp.defaultserver": "smtp1",
  "mail.smtpserver.smtp1.authMethod": 1,
  "mail.smtpserver.smtp1.hostname": "127.0.0.1",
  "mail.smtpserver.smtp1.port": 25,
  "mail.smtpserver.smtp1.try_ssl": 0,
  "mail.smtpservers": "smtp1",
  "mail.startup.enabledMailCheckOnce": false,
}

// Module exports

export function getPrefs() {
  return {
    ...prefsFirefox,
    ...prefsThunderbird,
  }
}

export { nonOverridablePreferences, coerceCLICustomPreference }
