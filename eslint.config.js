import globals from "globals"
import js from "@eslint/js"
import stylisticJs from "@stylistic/eslint-plugin-js"
import eslintPluginPrettierRecommended from "eslint-plugin-prettier/recommended"

export default [
  js.configs.recommended,
  {
    languageOptions: {
      ecmaVersion: 2023,
      sourceType: "module",
      globals: {
        ...globals.node,
      },
    },
    plugins: {
      "@stylistic/js": stylisticJs,
    },
    rules: {
      "@stylistic/js/quotes": [
        "error",
        "double",
        { avoidEscape: true, allowTemplateLiterals: true },
      ],
      "@stylistic/js/semi": ["error", "never"],
      "no-eval": "error",
      curly: ["error", "all"],
      "no-unused-vars": ["error", { args: "none", vars: "local" }],
      "@stylistic/js/max-len": [
        "error",
        {
          code: 99,
          tabWidth: 2,
          ignoreUrls: true,
        },
      ],
    },
  },
  eslintPluginPrettierRecommended,
]
