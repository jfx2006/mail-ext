# Mail-ext

This is an enhancer for Mozilla's [web-ext](https://github.com/mozilla/web-ext)
tool make Thunderbird mail extension development a little easier.

For the most part, this is accomplished by setting up a web-ext-config.js
file to convince web-ext that "Firefox" is "Thunderbird".

## Setup

- Create a new directory for your mail extension, change your shell
  to that directory. Run `npm init` or however you like to set up a
  basic nodejs project.
- `npm install --save-dev git+https://gitlab.com/jfx2006/mail-ext.git`
- You can now run it as such:
  ```
  npx mail-ext --help
  ```
- `npx mail-ext init` to create the config file and basic extension
- You can use the `mail-ext` command in your project as an
  [npm script](https://docs.npmjs.com/misc/scripts).
  Here is an example where the `--source-dir` argument specifies where to find
  the source code for your extension.

`package.json`

```json
"scripts": {
  "test": "mail-ext run --source-dir ./extension"
}
```

You can always pass in additional commands to your npm scripts using
the `--` suffix. For example, the previous script could specify the Thunderbird
version on the command line with this:

    npm run test -- --thunderbird=thunderbird-beta
  
## Commands

The standard web-ext commands work. Some have been modified to work
better with Thunderbird, some have not.

- [`run`](https://extensionworkshop.com/documentation/develop/web-ext-command-reference#web-ext-run)
    - Run the extension
- [`lint`](https://extensionworkshop.com/documentation/develop/web-ext-command-reference#web-ext-lint)
    - Validate the extension source
- [`sign`](https://extensionworkshop.com/documentation/develop/web-ext-command-reference#web-ext-sign)
    - **Disabled for now as Thunderbird does not require signed extensions**
- [`build`](https://extensionworkshop.com/documentation/develop/web-ext-command-reference#web-ext-build)
    - Create an extension package from source
- [`docs`](https://extensionworkshop.com/documentation/develop/web-ext-command-reference#web-ext-docs)
    - Open the `web-ext` documentation in a browser

Additional commands have been added (or will be soon):

- init
  - Create web-ext-config.js for mail-ext. Then generate a manifest.json
    file.

## Config file

You can use a `web-ext-config.js` file just like with `web-ext`.
**Note at some point I may start using `mail-ext-config.js` for the
default config file.**

```js
module.exports = {
    // Global options:
    verbose: true,
    // Command options:
    build: {
        overwriteDest: true,
        filename: "{name}-{version}.xpi",
    },
    sourceDir: "extension",
    ignoreFiles: ["**/test", "**/test/**"],
    run: {
        // must be in $PATH or a full path
        thunderbird: "thunderbird",
        thunderbirdProfile: "./test_profile",
        devtools: true,
        browserConsole: true,
    },
}
```

## Web-ext reference

This tool leverages web-ext by directly importing much of its code.
Therefore the [web-ext documentation](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext)
is a great resource. There is also the [command reference](https://extensionworkshop.com/documentation/develop/web-ext-command-reference).

## Should I Use It?

Yes! The web-ext tool is a great help building Firefox web extensions.
Now, you have the same for Thunderbird mail extensions!
