// This file is loaded when developers import 'mail-ext' in their own code.

import mailext from "./lib/main.js"

export default mailext
export const { cmd, main } = mailext
